import math
import cmath

class CVector( list ):

  def __init__( self, l=[] ):
    list.__init__( self, l )

  def append( self, r=0, i=0 ):
    super( CVector, self ).append( complex( r, i ) )

def print_float( n ):
  return ( '%12.6f' % n )

def print_complex( n ):
  return ( '( %12.6f, %12.6fi )' % ( n.real, n.imag ) )


class Fourier( object ):

  def __init__( self ):
    return None

  def vector( self, n ):
    v = CVector()
    while ( n > 0 ):
      v.append()
      n -= 1
    return v

  def dump( self, v ):
    for n in range( 0, len( v ) ):
      print ( "%3d :" % n ) + print_complex( v[ n ] ) + ( " Mod: %5.2f" % abs( v[ n ] ) ) + ( " Fase: %5.2f" % math.atan2( v[ n ].imag, v[ n ].real ) )

  def diff( self, u, v ):
    if ( len( u ) <> len( v ) ):
      print "Vettori non confrontabili"
      return -1
    else:
      for n in range( 0, len( u ) ):
        if ( u[ n ].real == 0 ):
          u_real = 0
        else:
          u_real = 100 * ( v[ n ].real - u[ n ].real ) / u[ n ].real
        if ( u[ n ].imag == 0 ):
          u_imag = 0
        else:
          u_imag = 100 * ( v[ n ].imag - u[ n ].imag ) / u[ n ].imag
      
        print ( "%3d" % n ) + " : " + print_complex( u[ n ] ) + " : " + print_complex( v[ n ] ) + ( " : D.real %12.4f" % u_real ) + "%" + ( " D.imag %12.4f" % u_imag ) + "%"

  def fill( self, v, f_callback ):
    N = len( v )
    for n in range( 0, N ):
      x = float( n ) / float( N )
      v[ n ] = f_callback( x ) 

  def _dft( self, a_f, k, a_t ):
    Xk = complex( 0, 0 )
    N  = len( a_t )
    phi = ( ( 2.0 * cmath.pi ) / N ) * k
    for n in range( 0, N ):
      theta = phi * n
      Xk = Xk + ( a_t[ n ] * complex( cmath.cos( theta ), -cmath.sin( theta ) ) )
    a_f[ k ] = Xk

    return 0

  def dft( self, a_f, a_t ):
    for k in range( 0, len( a_f ) ):
      self._dft( a_f, k, a_t )

  def _idft( self, a_t, n, a_f ):
    xn = complex( 0, 0 )
    N  = len( a_f )
    phi = ( ( 2.0 * cmath.pi ) / N ) * n
    for k in range( 0, N ):
      theta = phi * k
      xn = xn + ( a_f[ k ] * complex( cmath.cos( theta ), cmath.sin( theta ) ) )

    a_t[ n ] = xn / N

    return 0

  def idft( self, a_t, a_f ):
    for n in range( 0, len( a_t ) ):
      self._idft( a_t, n, a_f )
      
  def polar( self, R ):
    P = []
    for r in R:
      if not( type( r ) is complex ):
          return []
      P.append( cmath.polar( r ) )

    return P
