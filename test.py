from pyfourier  import *
from pychart    import *
import cmath
import random

#  ------------------------------------------------------------ 

def f_sen( x ):
  f = 1.0
  return complex( cmath.sin( f * 2.0 * cmath.pi * x ), 0.0 )

def f_ramp( x ):
  f = 2.0
  return complex( ( ( f * x ) % 1 ) - 0.5, 0.0 )

def f_rand( x ):
  f = 1.0
  return complex( 2.0 * ( random.random() - 0.5 ) , 0.0 )

def f_square( x ):
  f = 2.0
  if ( f * x ) % 1 <= 0.5:
    return complex( -0.5, 0.0 )
  else:
    return complex(  0.5, 0.0 )

def f_sen_sum( x ):
  f0 =  2.0
  f1 = 10.0
  return complex( ( cmath.sin( f0 * 2.0 * cmath.pi * x ) + 0.5 * cmath.sin( f1 * 2.0 * cmath.pi * x + ( cmath.pi / 2 ) ) ) / 2.0, 0.0 )

def f_sen_mul( x ):
  f0 =  2.0
  f1 = 10.0
  return complex( ( cmath.sin( f0 * 2.0 * cmath.pi * x ) * cmath.sin( f1 * 2.0 * cmath.pi * x ) ) / 1.0, 0.0 )

#  ------------------------------------------------------------ 

def draw_lines( draw, mapper, i, values, color ):
  if i > 0:
    p0 = mapper.map_v_c( ( i - 1, values[ i - 1 ] ) )
    p1 = mapper.map_v_c( ( i    , values[ i     ] ) )
    draw.line( [ p0, p1 ], color, 2 )

def draw_bars( draw, mapper, i, values, color ):
  pv = mapper.map_v_c( ( i    , values[ i ] ) )
  pz = mapper.map_v_c( ( i - 1, 0 ) )
  w = ( pz[ 0 ] - pv[ 0 ] ) / 2
  if values[ i ] >= 0:
    p0 = ( pv[ 0 ] - w, pz[ 1 ] - 1 )
    p1 = ( pv[ 0 ] + w, pv[ 1 ] + 1 )
  else:
    p0 = ( pv[ 0 ] - w, pz[ 1 ] + 1 )
    p1 = ( pv[ 0 ] + w, pv[ 1 ] - 1 )

  draw.rectangle( [ p0, p1 ], color, 2 )

#  ------------------------------------------------------------ 

print "--------------------------"
print "- Istanzo oggetto"
F=Fourier()
a_time = F.vector( 50 )
a_dft  = F.vector( 50 )

F.fill( a_time, f_sen_sum )
#F.dump( a_time )

#  ------------------------------------------------------------ 

a_time_real = []
for t in a_time:
    a_time_real.append( t.real )
g0 = Graph( a_time_real, draw_lines, ( 192, 0, 192, 255 ) )

print "--------------------------"
print "- Eseguo dft"
F.dft( a_dft, a_time )
#print "  a_dft"
#F.dump( a_dft )

a_dft_real = []
a_dft_imag = []
for f in a_dft:
    a_dft_real.append( f.real )
    a_dft_imag.append( f.imag )
g1 = Graph( a_dft_real, draw_bars, ( 192, 192, 0, 255 ) )
g2 = Graph( a_dft_imag, draw_bars, ( 192, 192, 0, 255 ) )

print "--------------------------"
print "- Calcolo coordinate polari array dft"
a_dft_polar = F.polar( a_dft )
a_dft_p_rad = []
a_dft_p_phi = []
for p in a_dft_polar:
    a_dft_p_rad.append( p[ 0 ] )
    a_dft_p_phi.append( p[ 1 ] )
    print "F: {0:4d} - Rag: {1:>8.4f} Phi: {2:>8.4f}".format( len( a_dft_p_rad )-1, p[ 0 ], p[ 1 ] )
g_dft_p_r = Graph( a_dft_p_rad, draw_bars, ( 0, 192, 192, 255 ) )
g_dft_p_p = Graph( a_dft_p_phi, draw_bars, ( 0, 192, 192, 255 ) )

## --------------------------------
##   Controllo
#
#print "--------------------------"
#print "- Copio vettore a_time"
#p = a_time
#a_time = F.vector( len( p ) )
#
#print "--------------------------"
#print "- Eseguo idft"
#F.idft( a_time, a_dft )
#print "  a_time"
#F.dump( a_time )
#
#print "--------------------------"
#print "Confronto a_time originale con a_time ricalcolato"
#F.diff( p, a_time )

##p0 = Panel( [ g0 ] )
##p1 = Panel( [ g1, g2 ] )
##p_dft_p = Panel( [ g_dft_p_r, g_dft_p_p ] )
##c0 = Chart( [ p0, p1, p_dft_p ] )

p0 = Panel( [ g0 ] )

#p1 = Panel( [ g1 ] )
#p2 = Panel( [ g2 ] )
p1 = Panel( [ g1, g2 ] )

#p_dft_p_r = Panel( [ g_dft_p_r ] )
#p_dft_p_p = Panel( [ g_dft_p_p ] )
p_dft_p = Panel( [ g_dft_p_r, g_dft_p_p ] )

#c0 = Chart( [ p0, p1, p2, p_dft_p_r, p_dft_p_p ] )
c0 = Chart( [ p0, p1, p_dft_p ] )

i = c0.render( 1600, 1200, 5 )
i.save( './img/img.png', 'PNG' )
